/*
  Copyright (c) 2008, STMicroelectronics
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of STMicroelectronics nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Contact: cedric.vincent@st.com
*/

#ifndef MEMINFO_H
#define MEMINFO_H

/** @mainpage
    The program mind.c is a very good example on how to use this library.
*/

#include <stdint.h> /* uint32_t, uint8_t, */

/** Structure of an entry in the meminfo table. */
struct meminfo {
	uint32_t origin;    /*!< Start address of the memory. */
	uint32_t length;    /*!< Length (in bytes) of the memory. */
	uint32_t flags;     /*!< Memory attributes. */
	uint32_t not_flags; /*!< Negated memory attributes. */
	uint8_t  name[32];  /*!< Name of the memory. */
} __attribute__((__packed__));

/** Extract the meminfo table from a file.
    @param  filename is the name of the file to read.
    @param  meminfo is a pointer to a non-allocated table of struct meminfo,
            this table is automatically calloc(3)-ated to store the extracted
	    meminfo table.
    @return the number of entries stored in the parameter meminfo. See the
            enum meminfo_error if this value is negative.
*/
extern int meminfo_read_file(char *filename, struct meminfo *meminfo[]);

/** Extract the meminfo table from a file descriptor.
    @param  fd is a file descriptor.
    @param  meminfo is a pointer to a non-allocated table of struct meminfo,
            this table is automatically calloc(3)-ated to store the extracted
	    meminfo table.
    @return the number of entries stored in the parameter meminfo. See the
            enum meminfo_error if this value is negative.
*/
extern int meminfo_read_fd(int fd, struct meminfo *meminfo[]);

/** Extract the meminfo table from an ELF.
    @param  elf is a pointer to a LibELF descriptor.
    @param  meminfo is a pointer to a non-allocated table of struct meminfo,
            this table is automatically calloc(3)-ated to store the extracted
	    meminfo table.
    @return the number of entries stored in the parameter meminfo. See the
            enum meminfo_error if this value is negative.
*/
extern int meminfo_read_elf(void *elf, struct meminfo *meminfo[]);

/** Enumeration of possible errors. */
enum meminfo_error {
	MEMINFO_ERROR_STRUCTURE = -1, /*<! The meminfo table is missing or incoherent. */
	MEMINFO_ERROR_SYSTEM    = -2, /*<! Check errno(3). */
	MEMINFO_ERROR_ELF       = -3, /*<! Check elf_errno. */
};

/** Extracted from ldlang.c:meminfo_flags(). Names are explicit enough. */
enum {
	MEMINFO_READONLY    = 0x1,
	MEMINFO_READWRITE   = 0x2,
	MEMINFO_EXECUTABLE  = 0x4,
	MEMINFO_ALLOCATABLE = 0x8,
	MEMINFO_INITIALIZED = 0x10
};

#endif /* MEMINFO_H */
