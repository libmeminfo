/*
  Copyright (c) 2008, STMicroelectronics
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of STMicroelectronics nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Contact: cedric.vincent@st.com
*/

#include <gelf.h>   /* gelf_*(3), elf*(3), */
#include <stdlib.h> /* NULL, */
#include <string.h> /* strcmp(3), */

#include "meminfo.h"

/* Iterate over sections in an ELF. */
#define foreach_section(elf, iterator) \
	while (((iterator) = elf_nextscn((elf), (iterator))) != NULL)

/* Iterate over data in a section. */
#define foreach_data(section, iterator) \
	while (((iterator) = elf_getdata((section), (iterator))) != NULL)

/* Used to copy a GElf_Sym variable into a Elf_Sym(32|64) one. */
#define COPY_SYM(a, b) \
	(a).st_name  = (b).st_name;  \
	(a).st_value = (b).st_value; \
	(a).st_size  = (b).st_size;  \
	(a).st_info  = (b).st_info;  \
	(a).st_other = (b).st_other; \
	(a).st_shndx = (b).st_shndx;

/* Return a symbol from its name. */
static int meminfo_get_symbol(Elf *elf, char *name, GElf_Sym *result)
{
	Elf_Scn *section = NULL;

	foreach_section(elf, section) {
		Elf_Data *data = NULL;
		void *status = NULL;
		GElf_Shdr header;

		/* Get the section header. */
		status = gelf_getshdr(section, &header);
		if (status == NULL)
			return -1;

		/* Filter out non-SYMTAB sections. */
		if (header.sh_type != SHT_SYMTAB)
			continue;

		/* Sanity check. */
		if (header.sh_entsize == 0)
			return -1;

		foreach_data(section, data) {
			 GElf_Sym *symbols = NULL;
			 size_t nb_symbols = 0;

			/* Filter SYM data. */
			if (data->d_type == ELF_T_SYM) {
				int i = 0;

				nb_symbols = (size_t)(header.sh_size / header.sh_entsize);
				symbols    = (GElf_Sym *)data->d_buf;

				/* Iterate over symbols in a data block. */
				for (i = 0; i < (int)nb_symbols; i++) {
					char *symbol_name = NULL;

					/* Copy the generic symbol into a specialized one. */
					switch(gelf_getclass(elf)) {
					case ELFCLASS32:
						COPY_SYM(*result, ((Elf32_Sym *)symbols)[i]);
						break;
					case ELFCLASS64:
						COPY_SYM(*result, ((Elf64_Sym *)symbols)[i]);
						break;
					default:
						return -1;
					}

					/* Get the string from its offset in a string table. */
					symbol_name = elf_strptr(elf, header.sh_link, result->st_name);
					if (symbol_name == NULL)
						return -1;

					if (strcmp(name, symbol_name) == 0)
						return 0;
				}
			}
		}
	}

	return -1;
}

#define CHANGE_ENDIANNESS(a) ((((a) & 0x000000FF) << 24) | \
			      (((a) & 0x0000FF00) << 8)  | \
			      (((a) & 0x00FF0000) >> 8)  | \
			      (((a) & 0xFF000000) >> 24))

/* Adjust the endianness of a meminfo entry, from the target format to the host one. */
static int meminfo_fix_endianness(Elf *elf, struct meminfo *meminfo)
{
	GElf_Ehdr header;

	/* Get the ELF header. */
	if (gelf_getehdr(elf, &header) == NULL)
		return -1;

	/* Adjust only if endiannesses are different. */
	if (header.e_ident[EI_DATA] != HOST_ENDIANNESS) {
		meminfo->origin    = CHANGE_ENDIANNESS(meminfo->origin);
		meminfo->length    = CHANGE_ENDIANNESS(meminfo->length);
		meminfo->flags     = CHANGE_ENDIANNESS(meminfo->flags);
		meminfo->not_flags = CHANGE_ENDIANNESS(meminfo->not_flags);
	}

	return 0;
}

int meminfo_read_elf(void *elf, struct meminfo *meminfo[])
{
	GElf_Sym meminfo_start;
	GElf_Sym meminfo_end;

	size_t meminfo_nb_entries = 0;
	size_t meminfo_offset   = 0;
	size_t meminfo_size = 0;

	Elf_Kind format  = ELF_K_NONE;
	Elf_Scn *section = NULL;
	Elf_Data *data   = NULL;
	GElf_Shdr section_header;
	GElf_Ehdr elf_header;
	int status = 0;
	int i = 0;

	/* Check that is not an ARchive file. */
	format = elf_kind(elf);
	if (format != ELF_K_ELF)
		return MEMINFO_ERROR_ELF;

	/* Get the symbol __meminfo_start. */
	status = meminfo_get_symbol(elf, "__meminfo_start", &meminfo_start);
	if (status < 0)
		return MEMINFO_ERROR_STRUCTURE;

	/* Get the symbol __meminfo_end. */
	status = meminfo_get_symbol(elf, "__meminfo_end", &meminfo_end);
	if (status < 0)
		return MEMINFO_ERROR_STRUCTURE;

	/* Sanity check: __meminfo_start and __meminfo_end must be
	   in the same section. */
	if (meminfo_start.st_shndx != meminfo_end.st_shndx)
		return MEMINFO_ERROR_STRUCTURE;

	meminfo_size = meminfo_end.st_value - meminfo_start.st_value;

	/* Sanity check: the size of the meminfo table must be
	   a multiple of sizeof(struct meminfo). */
	if ((meminfo_size % sizeof(struct meminfo)) != 0)
		return MEMINFO_ERROR_STRUCTURE;

	meminfo_nb_entries = meminfo_size / sizeof(struct meminfo);

	/* Get the section form its index. */
	section = elf_getscn(elf, meminfo_start.st_shndx);
	if (section == NULL)
		return MEMINFO_ERROR_ELF;

	/* Get the section header. */
	if (gelf_getshdr(section, &section_header) == NULL)
		return MEMINFO_ERROR_ELF;

	/* Get the ELF header. */
	if (gelf_getehdr(elf, &elf_header) == NULL)
		return MEMINFO_ERROR_ELF;

	/* Make meminfo's bounds section-relative except for a relocatable
	   object (because they are already section-relative in this case). */
	if (elf_header.e_type != ET_REL) {
		meminfo_start.st_value -= section_header.sh_addr;
		meminfo_end.st_value   -= section_header.sh_addr;
	}

	/* The user should free(3) this memory later. */
	*meminfo = (struct meminfo *)calloc(meminfo_nb_entries, sizeof(struct meminfo));
	if (*meminfo == NULL)
		return MEMINFO_ERROR_SYSTEM;

	foreach_data(section, data) {
		size_t blob_start = 0;
		size_t blob_end   = 0;
		size_t blob_size  = 0;

		/* Get the offset of the table within this buffer. */
		blob_start = meminfo_start.st_value - data->d_off;
		blob_end   = meminfo_end.st_value   - data->d_off;

		/* Check if meminfo_start is before this buffer.
		 * I can't use "blob_start < 0" here since it is unsigned. */
		if (meminfo_start.st_value < data->d_off)
			blob_start = 0;

		/* Check if meminfo_end is after this buffer. */
		if (blob_end > data->d_size)
			blob_end = data->d_size;

		blob_size = blob_end - blob_start;

		/* Copy the partial content if meminfo_start is not after
		   and meminfo_end is not before this buffer, and there is
		   something to copy. */
		if (blob_start < data->d_size && blob_end > 0 &&
		    blob_size > 0) {
			memcpy(*meminfo + meminfo_offset, data->d_buf + blob_start, blob_size);
			meminfo_offset += blob_size;
		}
	}

	/* Sanity check. */
	if (meminfo_offset != meminfo_size) {
		free(*meminfo);
		*meminfo = NULL;
		return MEMINFO_ERROR_STRUCTURE;
	}

	/* Avoid buffer overflows and adjust the endianness. */
	for (i = 0; i < meminfo_nb_entries; i++) {
		(*meminfo)[i].name[31] = 0;

		status = meminfo_fix_endianness(elf, &(*meminfo)[i]);
		if (status < 0)
			return MEMINFO_ERROR_ELF;
	}

	return meminfo_nb_entries;
}
