/*
  Copyright (c) 2008, STMicroelectronics
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of STMicroelectronics nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Contact: cedric.vincent@st.com
*/

#include <stdlib.h>   /* NULL, */
#include <stdio.h>    /* fprintf(3),  */
#include <string.h>   /* strlen(3), */
#include <libelf.h>   /* elf_errmsg(3), */
#include <inttypes.h> /* PRIu32, */
#include <unistd.h>   /* getopt(3), */

#include <meminfo.h> /* meminfo*, */
#include "license.h" /* LICENSE */

static void print_usage()
{
	fprintf(stderr, "MemINfo Dumper usage:\n\n");
	fprintf(stderr, "    mind [elf-file]\n");
	fprintf(stderr, "or\n");
	fprintf(stderr, "    mind [options]\n");
	fprintf(stderr, "\nOptions:\n");
	fprintf(stderr, "    -h    Print this help.\n");
	fprintf(stderr, "    -l    Print the license.\n");
}

static void print_license()
{
	char license[] = { LICENSE };
	fprintf(stderr, "%s\n", license);
}

extern char *optarg;
extern int optind;

int main(int argc, char **argv)
{
	struct meminfo *meminfo = NULL;
	char flag = '\0';
	int status = 0;
	int i = 0;

	/* Parse options. */
	while ((flag = getopt(argc, argv, "hl")) != (char) -1) {
		switch (flag) {
		case 'l':
			print_license();
			exit(EXIT_SUCCESS);
		case 'h':
		default:
			print_usage();
			exit(EXIT_FAILURE);
		}
	}

	if (argc - optind != 1) {
		print_usage();
		exit(EXIT_FAILURE);
	}

	status = meminfo_read_file(argv[optind], &meminfo);
	if (status <= 0) {
		switch (status) {
		case 0:
			fprintf(stderr, "The meminfo table is empty.\n");
			break;
		case MEMINFO_ERROR_STRUCTURE:
			fprintf(stderr, "Error: can't find meminfo entries.\n");
			break;
		case MEMINFO_ERROR_SYSTEM:
			fprintf(stderr, "Error system: "); perror(NULL);
			break;
		case MEMINFO_ERROR_ELF:
			fprintf(stderr, "Error ELF: %s\n", elf_errmsg(0));
			break;
		default:
			fprintf(stderr, "Error: I lost my mind...\n");
			break;
		}
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < status; i++) {
		printf("%-10s : ", (char *)meminfo[i].name);
		printf("origin = 0x%08"PRIX32", ",   meminfo[i].origin);
		printf("length = 0x%08"PRIX32", ",   meminfo[i].length);
		printf("flags = 0x%08"PRIX32", ",    meminfo[i].flags);
		printf("not_flags = 0x%08"PRIX32" ", meminfo[i].not_flags);

		printf("[");
		if (meminfo[i].flags & MEMINFO_READONLY)    printf("r");
		if (meminfo[i].flags & MEMINFO_READWRITE)   printf("w");
		if (meminfo[i].flags & MEMINFO_EXECUTABLE)  printf("x");
		if (meminfo[i].flags & MEMINFO_ALLOCATABLE) printf("a");
		if (meminfo[i].flags & MEMINFO_INITIALIZED) printf("i");
		if (meminfo[i].not_flags & MEMINFO_READONLY)    printf("!r");
		if (meminfo[i].not_flags & MEMINFO_READWRITE)   printf("!w");
		if (meminfo[i].not_flags & MEMINFO_EXECUTABLE)  printf("!x");
		if (meminfo[i].not_flags & MEMINFO_ALLOCATABLE) printf("!a");
		if (meminfo[i].not_flags & MEMINFO_INITIALIZED) printf("!i");
		printf("]\n");
	}

	free(meminfo);
	meminfo = NULL;

	exit(EXIT_SUCCESS);
}

