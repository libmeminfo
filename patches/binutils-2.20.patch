diff -Nawur binutils-2.20.orig//ld/ld.texinfo binutils-2.20/ld/ld.texinfo
--- binutils-2.20.orig//ld/ld.texinfo	2009-07-06 15:48:51.000000000 +0200
+++ binutils-2.20/ld/ld.texinfo	2010-07-06 13:08:25.000000000 +0200
@@ -3280,6 +3280,106 @@
 architecture of an object file by using the @code{objdump} program with
 the @samp{-f} option.
 @end ifclear
+
+@kindex CREATE_MEMINFO_TABLE
+@cindex memory regions
+@cindex meminfo
+@item CREATE_MEMINFO
+The command @code{CREATE_MEMINFO_TABLE} creates two symbols named
+``__meminfo_start'' and ``__meminfo_end'', and a table where
+entries are in the following format :
+
+@table @asis
+@item origin
+32 bits unsigned
+@item length
+32 bits unsigned
+@item flags
+32 bits unsigned
+@item not_flags
+32 bits unsigned
+@item name
+32 x 8 bits unsigned (including the terminating '\0' character)
+@end table
+
+@ref{MEMORY} describes flags (and the meaning of @code{not_flags}), their values
+can be extracted from the declaration of @code{struct sec} in @file{bfd.h}:
+
+@table @asis
+@item read-only (R)
+0x10
+@item read/write (W)
+0x40
+@item executable (X)
+0x20
+@item allocatable (A)
+0x01
+@item initialized (I or L)
+0x02
+@end table
+
+For instance, this memory description:
+@smallexample
+MEMORY
+@{
+   mem4data (wi) : ORIGIN = 0x00000000, LENGTH = 0x00010000
+   mem4text (xi) : ORIGIN = 0x40000000, LENGTH = 0x00300000
+@}
+@end smallexample
+will produce these entries :
+@smallexample
+origin    := 0x00000000
+length    := 0x00010000
+flags     := 0x40
+not_flags := 0x02
+name      := 'm' 'e' 'm' '4' 'b' 's' 's' '\0'x25
+
+origin    := 0x40000000
+length    := 0x00300000
+flags     := 0x20 | 0x40
+not_flags := 0x00
+name      := 'm' 'e' 'm' '4' 't' 'e' 'x' 't' '\0'x24
+@end smallexample
+
+This allows loaders and runners to check if the memory description
+(used to create an executable) is compatible with the target.
+
+The following fragment of linker-script is an example for loaders:
+@smallexample
+SECTIONS
+@{
+  [...]
+  .meminfo (NOLOAD) : @{ CREATE_MEMINFO_TABLE @}
+  [...]
+@}
+@end smallexample
+
+The following fragment of linker-script is an example for runners:
+@smallexample
+SECTIONS
+@{
+  [...]
+  .meminfo : @{ CREATE_MEMINFO_TABLE @} > .data
+  [...]
+@}
+@end smallexample
+
+The following fragment of C code is an example for runners:
+@smallexample
+extern void _meminfo_start;
+extern void _meminfo_end;
+
+int main(void)
+@{
+  return printf("meminfo: %p -> %p\n",
+                &_meminfo_start,
+                &_meminfo_end);
+@}
+@end smallexample
+
+Don't forget to use a single leading @code{_} (for C code) and to add @code{&}
+to read the value of the start/end of the memory information.
+
 @end table
 
 @node Assignments
diff -Nawur binutils-2.20.orig//ld/ldgram.y binutils-2.20/ld/ldgram.y
--- binutils-2.20.orig//ld/ldgram.y	2009-09-02 09:25:35.000000000 +0200
+++ binutils-2.20/ld/ldgram.y	2010-07-06 13:08:25.000000000 +0200
@@ -146,6 +146,7 @@
 %token CHIP LIST SECT ABSOLUTE  LOAD NEWLINE ENDWORD ORDER NAMEWORD ASSERT_K
 %token FORMAT PUBLIC DEFSYMEND BASE ALIAS TRUNCATE REL
 %token INPUT_SCRIPT INPUT_MRI_SCRIPT INPUT_DEFSYM CASE EXTERN START
+%token CREATE_MEMINFO_TABLE
 %token <name> VERS_TAG VERS_IDENTIFIER
 %token GLOBAL LOCAL VERSIONK INPUT_VERSION_SCRIPT
 %token KEEP ONLY_IF_RO ONLY_IF_RW SPECIAL
@@ -585,6 +586,8 @@
 		{ ldlex_script (); ldfile_open_command_file($2); }
 		statement_list_opt END
 		{ ldlex_popstate (); }
+	| CREATE_MEMINFO_TABLE
+		{ lang_add_meminfo_table (); }
 	;
 
 statement_list:
diff -Nawur binutils-2.20.orig//ld/ldlang.c binutils-2.20/ld/ldlang.c
--- binutils-2.20.orig//ld/ldlang.c	2009-10-14 11:26:29.000000000 +0200
+++ binutils-2.20/ld/ldlang.c	2010-07-06 13:08:25.000000000 +0200
@@ -6554,6 +6554,75 @@
   new_statement (attribute, sizeof (lang_statement_header_type), stat_ptr);
 }
 
+static inline flagword
+meminfo_flags (flagword mem_flags)
+{
+  flagword flags = 0;
+  enum
+  {
+    MEMINFO_READONLY    = 0,
+    MEMINFO_READWRITE   = 1,
+    MEMINFO_EXECUTABLE  = 2,
+    MEMINFO_ALLOCATABLE = 3,
+    MEMINFO_INITIALIZED = 4
+  };
+
+  flags |= (mem_flags & SEC_READONLY) != 0 ? 1 << MEMINFO_READONLY    : 0;
+  flags |= (mem_flags & SEC_DATA)     != 0 ? 1 << MEMINFO_READWRITE   : 0;
+  flags |= (mem_flags & SEC_CODE)     != 0 ? 1 << MEMINFO_EXECUTABLE  : 0;
+  flags |= (mem_flags & SEC_ALLOC)    != 0 ? 1 << MEMINFO_ALLOCATABLE : 0;
+  flags |= (mem_flags & SEC_LOAD)     != 0 ? 1 << MEMINFO_INITIALIZED : 0;
+
+  return flags;
+}
+
+void lang_add_meminfo_table ()
+{
+  lang_memory_region_type *mem = NULL;
+  const size_t max_length = 31; /* +1 for the terminating '\0' */
+
+  if (link_info.relocatable != 0)
+    return;
+
+  lang_add_assignment (exp_assop ('=', "__meminfo_start", exp_nameop (NAME, ".")));
+
+  for (mem = lang_memory_region_list; mem != NULL; mem = mem->next)
+    {
+      unsigned int i = 0;
+      char name[max_length + 1];
+      lang_memory_region_name *region_name;
+
+      /* Skip the DEFAULT_MEMORY_REGION */
+      for (region_name = &mem->name_list; region_name != NULL; region_name = region_name->next)
+	if (strcmp (region_name->name, DEFAULT_MEMORY_REGION) == 0)
+	  goto skip;
+
+      memset (name, 0, sizeof (name));
+
+      lang_add_data (LONG, exp_intop (mem->origin));
+      lang_add_data (LONG, exp_intop (mem->length));
+      lang_add_data (LONG, exp_intop (meminfo_flags(mem->flags)));
+      lang_add_data (LONG, exp_intop (meminfo_flags(mem->not_flags)));
+
+      if (strlen (mem->name_list.name) > max_length)
+	einfo (_("%P: warning: the name of memory region `%s'"
+		 "will be truncated in the meminfo table\n"), mem->name_list.name);
+
+      strncpy (name, mem->name_list.name, max_length);
+
+      /* name[max_length] = '\0';
+	 Useless, because of the previous memset(). CV */
+
+      for (i = 0; i < max_length + 1; i++)
+	lang_add_data (BYTE, exp_intop(name[i]));
+
+    skip:
+      ;
+    }
+  
+  lang_add_assignment (exp_assop ('=', "__meminfo_end", exp_nameop (NAME, ".")));
+}
+
 void
 lang_startup (const char *name)
 {
diff -Nawur binutils-2.20.orig//ld/ldlang.h binutils-2.20/ld/ldlang.h
--- binutils-2.20.orig//ld/ldlang.h	2009-09-14 13:43:29.000000000 +0200
+++ binutils-2.20/ld/ldlang.h	2010-07-06 13:08:25.000000000 +0200
@@ -512,6 +512,8 @@
   (const char *);
 extern void lang_float
   (bfd_boolean);
+extern void lang_add_meminfo_table
+  (void);
 extern void lang_leave_output_section_statement
   (fill_type *, const char *, lang_output_section_phdr_list *,
    const char *);
diff -Nawur binutils-2.20.orig//ld/ldlex.l binutils-2.20/ld/ldlex.l
--- binutils-2.20.orig//ld/ldlex.l	2009-10-14 16:51:32.000000000 +0200
+++ binutils-2.20/ld/ldlex.l	2010-07-06 13:08:25.000000000 +0200
@@ -296,6 +296,7 @@
 <BOTH,SCRIPT>"SORT_BY_NAME"		{ RTOKEN(SORT_BY_NAME); }
 <BOTH,SCRIPT>"SORT_BY_ALIGNMENT"	{ RTOKEN(SORT_BY_ALIGNMENT); }
 <BOTH,SCRIPT>"SORT"			{ RTOKEN(SORT_BY_NAME); }
+<BOTH,SCRIPT>"CREATE_MEMINFO_TABLE"	{ RTOKEN(CREATE_MEMINFO_TABLE);}
 <EXPRESSION,BOTH,SCRIPT>"NOLOAD"	{ RTOKEN(NOLOAD);}
 <EXPRESSION,BOTH,SCRIPT>"DSECT"		{ RTOKEN(DSECT);}
 <EXPRESSION,BOTH,SCRIPT>"COPY"		{ RTOKEN(COPY);}
diff -Nawur binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.d binutils-2.20/ld/testsuite/ld-scripts/meminfo.d
--- binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.d	1970-01-01 01:00:00.000000000 +0100
+++ binutils-2.20/ld/testsuite/ld-scripts/meminfo.d	2010-07-06 13:08:26.000000000 +0200
@@ -0,0 +1,13 @@
+#source: meminfo.s
+#ld: -T meminfo.t
+#objdump: -s --section=.meminfo
+
+.*:     file format .*
+
+Contents of section .meminfo:
+ 0000 00000000 00(01)?00(01)?00 (12)?000000(12)? 00000000  ................
+ 0010 6d656d34 64617461 00000000 00000000  mem4data........
+ 0020 00000000 00000000 00000000 00000000  ................
+ 0030 (40)?000000(40)? 00(30)?00(30)?00 (14)?000000(14)? 00000000  ...@..0.........
+ 0040 6d656d34 74657874 00000000 00000000  mem4text........
+ 0050 00000000 00000000 00000000 00000000  ................
diff -Nawur binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.s binutils-2.20/ld/testsuite/ld-scripts/meminfo.s
--- binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.s	1970-01-01 01:00:00.000000000 +0100
+++ binutils-2.20/ld/testsuite/ld-scripts/meminfo.s	2010-07-06 13:08:26.000000000 +0200
@@ -0,0 +1 @@
+
diff -Nawur binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.t binutils-2.20/ld/testsuite/ld-scripts/meminfo.t
--- binutils-2.20.orig//ld/testsuite/ld-scripts/meminfo.t	1970-01-01 01:00:00.000000000 +0100
+++ binutils-2.20/ld/testsuite/ld-scripts/meminfo.t	2010-07-06 13:08:26.000000000 +0200
@@ -0,0 +1,10 @@
+MEMORY
+{
+  mem4data (wi) : ORIGIN = 0x00000000, LENGTH = 0x00010000
+  mem4text (xi) : ORIGIN = 0x40000000, LENGTH = 0x00300000
+}
+
+SECTIONS
+{
+  .meminfo (NOLOAD) : { CREATE_MEMINFO_TABLE }
+}
diff -Nawur binutils-2.20.orig//ld/testsuite/ld-scripts/script.exp binutils-2.20/ld/testsuite/ld-scripts/script.exp
--- binutils-2.20.orig//ld/testsuite/ld-scripts/script.exp	2009-09-02 09:25:41.000000000 +0200
+++ binutils-2.20/ld/testsuite/ld-scripts/script.exp	2010-07-06 13:08:26.000000000 +0200
@@ -136,3 +136,5 @@
         xpass "REGION_ALIAS: $test_script"
     }
 }
+
+run_dump_test meminfo
