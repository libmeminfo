/*
  Copyright (c) 2008, STMicroelectronics
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of STMicroelectronics nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Contact: cedric.vincent@st.com
*/

#include <gelf.h>   /* elf*(3), */
#include <stdlib.h> /* NULL, */

#include "meminfo.h"

int meminfo_read_fd(int fd, struct meminfo *meminfo[])
{
	size_t meminfo_nb_entries = 0;
	unsigned version = EV_NONE;
	Elf *elf = NULL;

	/* This check is mandatory to initialize the libelf. */
	version = elf_version(EV_CURRENT);
	if (version == EV_NONE)
		return MEMINFO_ERROR_ELF;

	/* Create an Elf descriptor from the file descriptor. */
	elf = elf_begin(fd, ELF_C_READ, NULL);
	if (elf == NULL)
		return MEMINFO_ERROR_ELF;

	meminfo_nb_entries = meminfo_read_elf(elf, meminfo);

	/* Ignore errors when closing the ELF descriptor. */
	(void) elf_end(elf);

	return meminfo_nb_entries;
}
